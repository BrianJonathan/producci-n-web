<!doctype html>
<html>

<head>
    <?php
    require_once("../clases/clasecuenta.php");
    require_once("../clases/claseusuario.php");
    session_start();

    if (!isset($_SESSION["usuario"])) :
        header("Location:../index.php?estado=prohibido");
        die();
    endif;

    require_once("../arrays.php");
    require_once("../funciones.php");

    ?>

    <link rel="icon" href="../img/zapatosHeader.jpg">
    <link rel="stylesheet" href="stylee.css">

    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">


</head>

<body>
    <div class="background16">
        <?php include "secciones/navegador.php"; ?>

        <?php

        if (!empty($_GET["secciones"])) {
            $secciones = $_GET["secciones"];

            if ($secciones == "inicio") :
                require("modulo/inicio.php");

            elseif ($secciones == "contacto") :
                require("modulo/contacto.php");

            elseif ($secciones == "eliminarSQL") :
                require("secciones/eliminarSQL.php");

            elseif ($secciones == "altaarticulo") :
                require("secciones/altaarticulo.php");

            elseif ($secciones == "modifarticulo") :
                require("secciones/modifarticulo.php");

            elseif ($secciones == "editar") :
                require("secciones/editar.php");

            else :

                require("modulo/error404.php");

            endif;
        } else {

            include "modulo/inicio.php";
        }

        ?>

        <?php include "../modulos/footer.php"; ?>
    </div>
</body>

</html>