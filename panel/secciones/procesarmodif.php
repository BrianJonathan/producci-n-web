<?php

    require_once("../../clases/clasearticulo.php");
    require_once("../../clases/clasecuenta.php");
    require_once("../../clases/claseSQL.php");

    if(empty($_POST["id"])):

        header("Location: ../index.php?secciones=modifarticulo");
    endif;

    $id_art = $_POST["id"];
    $titulo = $_POST["titulo"];
    $precio = $_POST["precio"];
    $imagen = $_FILES["imagen"];
    $carpeta = "../../imagenes";
    $carpetaRUTA = "imagenes";

    if($imagen["type"] == "image/jpeg"):
        $formato = "jpg";

    elseif($imagen["type"] == "image/png"):
        $formato = "png";

    elseif($imagen["type"] == "image/gif"):
        $formato = "gif";
    
    else:
        
        header("Location: ../index.php?secciones=editar&estado=formato");
        die();

    endif;

    $carpetaeliminar = "$carpetaRUTA/$titulo/$titulo.$formato";

    Articulo::eliminardirarticulo($carpetaeliminar);

    $nombreLimpio = htmlentities($titulo);
    $nombreLimpio = strtolower($nombreLimpio);
    $nombreLimpio = str_replace(" ","",$nombreLimpio);

    if(!is_dir("$carpeta/$nombreLimpio")){
        
        mkdir("$carpeta/$nombreLimpio");
        
    }

    move_uploaded_file($imagen["tmp_name"],"$carpeta/$nombreLimpio/$nombreLimpio.$formato");

    $rutaimagen = "$carpetaRUTA/$nombreLimpio/$nombreLimpio.$formato";

    Articulo::registroarticulomodif($id_art);

    Articulo::modificararticulo($id_art,$titulo,$precio,$rutaimagen);

    header("Location: ../index.php?secciones=modifarticulo&estado=modificado");