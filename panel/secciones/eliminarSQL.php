<?php

require_once("../../clases/claseusuario.php");
require_once("../../clases/clasecuenta.php");
require_once("../../clases/claseSQL.php");

$id = $_POST["id"];

if($id == 1){
    
    header("Location:../index.php?modulo=inicio&estado=admin");
    die();
    
}else{

    SQL::deleteMySQL($id);
    SQL::deleteMySQLA($id);
    SQL::deleteMySQLRA($id);

    header("Location:../index.php?modulo=inicio&estado=eliminado");
    
}

