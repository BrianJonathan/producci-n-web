<?php

require_once("../../clases/clasearticulo.php");
require_once("../../clases/clasecuenta.php");
require_once("../../clases/claseSQL.php");

    $info = Articulo::verificaralta($_POST["titulo"],$_POST["precio"]);

    $imagen = $_FILES["imagen"];
    $carpeta = "../../imagenes";
    $carpetaRUTA = "imagenes";

    if($imagen["type"] == "image/jpeg"):
    
        $formato = "jpg";

    elseif($imagen["type"] == "image/png"):

        $formato = "png";

    elseif($imagen["type"] == "image/gif"):

        $formato = "gif";
    
    else:
        
        header("Location: ../index.php?secciones=altaarticulo&estado=formato");
        die();

    endif;

    $nombreLimpio = htmlentities($info['titulo']);

    $nombreLimpio = strtolower($nombreLimpio);

    $nombreLimpio = str_replace(" ","",$nombreLimpio);
    
    if(!is_dir("$carpeta/$nombreLimpio")){
        
        mkdir("$carpeta/$nombreLimpio");
        
    }

    move_uploaded_file($imagen["tmp_name"],"$carpeta/$nombreLimpio/$nombreLimpio.$formato");

    $rutaimagen = "$carpetaRUTA/$nombreLimpio/$nombreLimpio.$formato";

    Articulo::registroarticuloalta();

    Articulo::altaarticulo($info['titulo'],$info['precio'],$rutaimagen);

    header("Location: ../index.php?secciones=altaarticulo&estado=cargado&titulo=".$info['titulo']);