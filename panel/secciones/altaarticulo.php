<?php

if (!empty($_GET["estado"])) :
    $estado = $_GET["estado"];

    if ($estado == "vacio") :
        $mensaje = "El campo imagen y nombre son obligatorios";

    elseif ($estado == "formato") :
        $mensaje = "La imagen debe ser en formato JPG, PNG o GIF.";

    elseif ($estado == "cargado") :
        $mensaje = "El articulo fue cargado exitosamente";
    else :

        $mensaje = "No cambiar la URL";
    endif;
?>

    <div>
        <p class="subido"> <?= $mensaje ?> </p>
    </div>

<?php

endif;

?>
<div class="background">
    <div class="centrar">
        <h2 class="mb-5 text-left">Alta de producto</h2>
        <form method="POST" action="secciones/procesaralta.php" enctype="multipart/form-data">

            <div class="form-group text-left">
                <label for="exampleInputEmail1">Nombre articulo</label>
                <input type="text" class="form-control mb-5" id="id" placeholder="Ingrese titulo del articulo" name="titulo">
            </div>

            <div class="form-group text-left">
                <small id="emailHelp" class="form-text text-muted">Ingrese una imagen de su articulo.</small>
                <input type="file" class="form-control-file mb-3" name="imagen" accept="image/png, image/jpeg, image/gif">
            </div>
            <br />
            <div class="form-group text-left">
                <input type="text" class="form-control mb-3" id="precio" placeholder="Ingrese precio del articulo" name="precio">
            </div>
            <button class="btn btn-primary" type="submit">Subir</button>
        </form>
    </div>
</div>