<?php

    require_once("../clases/clasearticulo.php");
    require_once("../clases/clasecuenta.php");
    require_once("../clases/claseSQL.php");

    if(!empty($_GET["estado"])):
        $estado = $_GET["estado"];

    if($estado == "vacio"):
        $mensaje = "El campo imagen y nombre son obligatorios";
    elseif($estado == "formato"):
        $mensaje = "La imagen debe ser en formato JPG, PNG o GIF.";
    elseif($estado == "cargado"):
        $mensaje = "El articulo fue cargado exitosamente";
    else:

        $mensaje = "No cambiar la URL";

    endif;

?>

    <div>
        <p class="subido"> <?= $mensaje ?> </p>
    </div>

<?php

    endif;

?>

<div class="centrar">
    <?php
	   Articulo::formmodifarticulos($_GET["id_art"]);
    ?>
</div>