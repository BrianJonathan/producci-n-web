<main class="objeto">
    <div class="background">
        <div class="titulos">
            <?php
            require_once("../clases/clasearticulo.php");
            require_once("../clases/clasecuenta.php");
            require_once("../clases/claseSQL.php");

            if (!empty($_GET["estado"])) :
                $estado = $_GET["estado"];

                if ($estado == "eliminado") :
                    $mensaje = "La cuenta ha sido eliminada";
                elseif ($estado == "admin") :
                    $mensaje = "No se puede eliminar al admin";
                else :
                    $mensaje = "No tocar la URL";
                endif;
            ?>

                <div>
                    <p class="subido"> <?= $mensaje ?> </p>
                </div>

            <?php endif; ?>
            <table class="tabla">
                <caption id="titulo">
                    Panel de control
                </caption>

                <?php
                //print_r(Cuenta::administrardatos());
                //die();

                if ($_SESSION["usuario"]->id == 1) {

                    Cuenta::admintabla();
                ?>

                    <table class="tabla">
                        <caption id="titulo">
                            Eliminar una cuenta
                        </caption>
                        <section id="borrarusuario">
                        </section>

                    <?php
                    Cuenta::admindelete();
                } else {
                    Cuenta::tablausuario();
                    ?>

                        <table class="tabla">
                            <caption id="titulo">
                                Articulos
                            </caption>
                            <section id="articulos">
                            </section>

                        <?php
                        Articulo::tablaarticulos();
                    }
                        ?>
                        </table>
                    </table>
            </table>
        </div>
    </div>
</main>