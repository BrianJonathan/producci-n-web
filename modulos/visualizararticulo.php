<?php

require_once("clases/clasearticulo.php");
require_once("clases/clasecuenta.php");
require_once("clases/claseSQL.php");

?>
<div class="background16">
    <div class="centrar">
        <?php
        Articulo::visualizararticulo($_GET["id_art"]);

        $pagina = $_GET["id_art"];
        ?>

        <br />
        <div class="row">
            <div class="button3">
                <?php
                if ($pagina < Articulo::buscarmaxID()) {
                    $pagina = $pagina + 1;
                } else if ($pagina == Articulo::buscarmaxID() - 1) {
                    $pagina = Articulo::buscarmaxID() - 1;
                } else {
                    $pagina = Articulo::buscarmaxID() + 1;
                }
                ?>
                <a href="index.php?modulos=visualizararticulo&id_art=<?= $pagina ?>" class="btn btn-primary ml-5">Pag sig</a>
            </div>  

            <br />

            <div class="button3">
                <?php
                if ($pagina > 2) {
                    $paginaanterior = $pagina - 2;
                } else {
                    $paginaanterior = 1;
                }
                ?>
                <a href="index.php?modulos=visualizararticulo&id_art=<?= $paginaanterior ?>" class="btn btn-primary ml-5">Pag ant</a>
            </div>
        </div>
    </div>
</div>