<link rel="stylesheet" href="../style.css">
<div class="background">
  <div class="container">
    <div class="row">
      <h2 id="titulo">
        Articulos
      </h2>

      <?php
      require_once("clases/clasearticulo.php");
      require_once("clases/claseSQL.php");

      $pagina = $_GET['pagina'];

      $contador = 0;

      $limite = 11;

      if (!isset($_GET['pagina']) || $_GET['pagina'] == 1) {

        $pagina = 1;

        Articulo::tablaarticulospublica($pagina, $contador, $limite);

        die();
      }

      if (isset($_GET['pagina'])) {

        $contador += 10;
        $limite += 10;

        Articulo::tablaarticulospublica($pagina, $contador, $limite);
      }
      ?>
    </div>
  </div>
</div>