<div class="background16">
    <div class="centrar">
        <h2>Dejenos sus Comentarios!!</h2>
        <form method="post" action="index.php?modulos=validacionContacto">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <small for="email text-left">Email</small>
                    <input type="email" class="form-control text-left" name="email" placeholder="Email">
                </div>
            </div>

            <div class="form-group col-md-4">
                <label for="sector">Elija una opción</label>
                <select name="sector" class="form-control" required>
                    <option selected>Otro</option>
                    <option>Recursos Humanos</option>
                    <option>Departamento de reclamos</option>
                    <option>Distribuidores oficiales</option>
                </select>

                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Descripción</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                    <button type="submit" class="btn btn-primary">Enviar</button>

                    <?php exit; ?>
                    <!-- <div class="alert alert-success" role="alert">
                        Mensaje enviado con exito
                    </div> -->
                </div>

            </div>
        </form>
    </div>
</div>