<!doctype html>
<html>

<head>

    <?php
    session_start();
    require_once("arrays.php");
    require_once("funciones.php");
    require_once("global.php");
    ?>

    <link rel="icon" href="img/zapatosHeader.jpg">
    <link rel="stylesheet" href="style.css">

    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</head>

<body>

    <?php include "modulos/nav.php"; ?>

    <?php
    if (!empty($_GET["modulos"])) {
        $modulos = $_GET["modulos"];

        if ($modulos == "pagina") :
            require("modulos/pagina.php");

        elseif ($modulos == "login") :
            require("modulos/login.php");

        elseif ($modulos == "contacto") :
            require("modulos/contacto.php");

        elseif ($modulos == "validacionContacto") :
            require("modulos/validacionContacto.php");

        elseif ($modulos == "mostrararticulos") :
            require("modulos/mostrararticulos.php");

        elseif ($modulos == "visualizararticulo") :
            require("modulos/visualizararticulo.php");

        elseif ($modulos == "registrarse") :
            require("modulos/registrarse.php");

        elseif ($modulos == "panel") :
            require("modulos/panel.php");

        else :

            require("modulos/error404.php");

        endif;
    } else {
        require("modulos/pagina.php");
    }
    ?>

    <?php include "modulos/footer.php"; ?>

</body>

</html>