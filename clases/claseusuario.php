<?php

class Usuario{

    public $id;
    public $email;
    public $password;
    
    public function __construct($id = null,$email = null,$password = null){

        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        
    }
    
    public function verificar($emails,$passwords){
        
        $respuesta = json_decode(SQL::selectMySQL());
        
        foreach($respuesta as $pos => $usuario):
        
        if($usuario->email == $emails):
        
            if(password_verify($passwords,$usuario->password)):

                $posicion = $pos;

            endif;
        endif;
            
    endforeach;

    if(!isset($posicion)):
        
        header("Location:index.php?modulos=login&error=1");
        die();
    endif;
        
    return $posicion;
        
    }
    
    public function administrarusuario($posicion){
        
        $respuesta = json_decode(SQL::selectMySQL());
        
        $user = new Usuario();
        $user->id = $respuesta[$posicion]->id;
        $user->email = $respuesta[$posicion]->email;
        $user->password = $respuesta[$posicion]->password;

        $_SESSION["usuario"] = $user;
        
        $id = $user->id;
        
        return $id;
        
    }
    
    public function administraradmin($posicion){
        
        $respuesta = json_decode(SQL::selectMySQL());
        
        $admin = new Usuario();
        $admin->id = $respuesta[$posicion]->id;
        $admin->email = $respuesta[$posicion]->email;
        $admin->password = $respuesta[$posicion]->password;

        $_SESSION["usuario"] = $admin;
        
        return $admin;
        
    }
    
}