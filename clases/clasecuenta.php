<?php

    class Cuenta{
        
        public $id;
        public $email;
        public $ultimaconexion;
    
    public function __construct($id = null,$email = null,$ultimaconexion = null){

        $this->id = $id;
        $this->email = $email;
        $this->ultimaconexion = $ultimaconexion;   
    }
        
    public function conexiones($id,$emails){
        
        $link = SQL::conectarMySQL();
        $fechaactual = date('Y-m-d H:m:s');
        $query = "INSERT INTO datos(ID_CUENTA,EMAIL_USUARIO,ULTIMA_CONEXION)
                  VALUES ('".$id."','".$emails."','".$fechaactual."');";
        
        //echo $query;
        //die();
        
        $result = mysqli_query($link,$query);
        
        mysqli_close($link);   
        
    }
        
    public function selectdatos(){
        
        $link = SQL::conectarMySQL();        
        $query = 'SELECT ID_CUENTA,EMAIL_USUARIO,ULTIMA_CONEXION FROM datos;';        
        $result = mysqli_query($link,$query);        
        $respuestadatos = array();
        
        if ( 0 < mysqli_num_rows($result) ) {
           
            while ($row = mysqli_fetch_array($result)) {
                
                array_push($respuestadatos,array("id"=>$row["ID_CUENTA"],
                                                 "email"=>$row["EMAIL_USUARIO"],
                                                "ultima_conexion"=>$row["ULTIMA_CONEXION"]));
        
            }
            
        } else {
        
            echo 'No hay resultados';
            
        } 
        
        $respuestadata = json_encode($respuestadatos);
        
        return $respuestadata;
        
        //mysqli_close($link);
    }
    
    public function administrardatos(){
        
        $respuesta = json_decode(Cuenta::selectdatos());
        
        foreach($respuesta as $pos => $usuario):

                $posicion = $pos;
            
        endforeach;

        if(!isset($posicion)):
        
            header("Location:index.php?modulos=login&error=1");
            die();
        
        endif;
        
        $data = new Cuenta();
        $data->id = $respuesta[$posicion]->id;
        $data->email = $respuesta[$posicion]->email;
        $data->ultima_conexion = $respuesta[$posicion]->ultima_conexion;

        $_SESSION["usuario"] = $data;
        
        return $data;
         
    }
        
    public function tablausuario(){
        
        $link = SQL::conectarMySQL();
        $datos = self::administrardatos();
        $query = 'SELECT ID_CUENTA,EMAIL_USUARIO,ULTIMA_CONEXION FROM datos;';
        $result = mysqli_query($link,$query);

        if ($result->num_rows > 0) {
            
        echo '<table class="tabla">
                <tr class="color1">
                    <th>ID</th>
                    <th>Email</th>
                    <th>Conexiones</th>
                </tr>';
            
            while($row = $result->fetch_assoc()) {
                
                if($row["ID_CUENTA"] == $datos->id){
                
                echo '<tr class="color2">
                        <td>'.$row["ID_CUENTA"].'</td>
                        <td>'.$row["EMAIL_USUARIO"].'</td>
                        <td>'.$row["ULTIMA_CONEXION"].'</td>
                       </tr>';
                    
                } 
            }
        echo "</table>";
            
        } else {
        echo "No hay resultados";
        }
        
        //mysqli_close($link);
    }
        
    public function admintabla(){
        
        $link = SQL::conectarMySQL();
        $query = 'SELECT ID_CUENTA,EMAIL_USUARIO,ULTIMA_CONEXION FROM datos;';
        $result = mysqli_query($link,$query);

        if ($result->num_rows > 0) {
            
        echo '<table class="tabla">
                <tr class="color1">
                    <th>ID</th>
                    <th>Email</th>
                    <th>Conexiones</th>
                </tr>';
            
            while($row = $result->fetch_assoc()) {
                
                echo '<tr class="color2">
                        <td>'.$row["ID_CUENTA"].'</td>
                        <td>'.$row["EMAIL_USUARIO"].'</td>
                        <td>'.$row["ULTIMA_CONEXION"].'</td>
                      </tr>';
                
            }
            
        echo "</table>";
            
        } else {
        echo "No hay resultados";
        }
        
    }
        
    public function admindelete(){
       
        $link = SQL::conectarMySQL();
        $query = 'SELECT ID_USUARIO,EMAIL_USUARIO FROM registro;';
        $result = mysqli_query($link,$query);

        if ($result->num_rows > 0) {
            
        echo '<table class="tabla">
                <tr class="color1">
                    <th>ID</th>
                    <th>Email</th>
                    <th>ACCIONES</th>
                </tr>';
            
            while($row = $result->fetch_assoc()) {
                
                if($row["ID_USUARIO"] != 1){
                    
                echo '<tr class="color2">
                        <td>'.$row["ID_USUARIO"].'</td>
                        <td>'.$row["EMAIL_USUARIO"].'</td>
                        <td>
                            <form action="secciones/eliminarSQL.php" method="post"> 
                                <input 
                                    type="hidden" 
                                    name="id" 
                                    value="'.$row["ID_USUARIO"].'">
                                <button type="submit" class="button1">Eliminar</button>
                            </form>
                        </td>
                       </tr>';
   
                } 
            }
            
        echo "</table>";

        } else {

        echo "No hay resultados";

        }
        
        mysqli_close($link);
        
    }
           
}