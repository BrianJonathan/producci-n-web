<?php

class Articulo
{

    public function tablaarticulospublica($pagina, $contador, $limite)
    {

        $ultimoid = self::buscarmaxID();

        if ($pagina > 1) {
            $paginaanterior = $pagina - 1;
        } else {
            $paginaanterior = 1;
        }

        if ($pagina > 2) {
            $contador += 10;
            $limite += 10;
        }

        $link = SQL::conectarMySQL();
        $query = 'SELECT ID_ARTICULO,ID_CUENTA,TITULO,PRECIO,IMAGEN FROM articulos;';
        $result = mysqli_query($link, $query);

        if ($result->num_rows > 0) {

            echo '<table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">TITULO</th>
                            <th scope="col">IMAGEN</th>
                            <th scope="col">ACCION</th>
                        </tr>
                    </thead> ';

            while ($row = $result->fetch_assoc()) {

                if ($row["ID_ARTICULO"] > $contador && $row["ID_ARTICULO"] < $limite) {
                    echo '
                    <tr>
                        <td>' . $row["TITULO"] . '</td>
                        <td><img src="' . $row["IMAGEN"] . '" class="img-thumbnail" alt=""  width="50"></td>
                        <td><a href="index.php?modulos=visualizararticulo&id_art=' . $row["ID_ARTICULO"] . '" class="btn btn-primary">Ver</a></td>
                    </tr>';

                    $contador++;
                }
            }

            echo "</table>";

            /*
        echo "//Contador: $contador";
        echo "//Limite: $limite";
        echo "//Pagina: $pagina";
        echo "//Ultimo id: $ultimoid";
        */

            if ($limite >= $ultimoid) {
                $pagina;
            } else {
                $pagina++;
            }
        } else {
            echo "No hay resultados";
        }
        //mysqli_close($link);
    }

    public function tablaarticulos()
    {

        $link = SQL::conectarMySQL();
        $datos = Cuenta::administrardatos();
        $query = 'SELECT ID_ARTICULO,ID_CUENTA,TITULO,PRECIO,IMAGEN FROM articulos;';
        $result = mysqli_query($link, $query);

        if ($result->num_rows > 0) {

            echo '<table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">ID ARTICULO</th>
                            <th scope="col">ID USUARIO</th>
                            <th scope="col">TITULO</th>
                            <th scope="col">PRECIO</th>
                            <th scope="col">IMAGEN</th>
                        </tr>
                    </thead>';

            while ($row = $result->fetch_assoc()) {

                if ($row["ID_CUENTA"] == $datos->id) {

                    echo '
                    <tbody>
                        <tr>
                            <td>' . $row["ID_ARTICULO"] . '</td>
                            <td>' . $row["ID_CUENTA"] . '</td>
                            <td>' . $row["TITULO"] . '</td>
                            <td>' . $row["PRECIO"] . '</td>
                            <td><img src="../' . $row["IMAGEN"] . '" class="img-thumbnail" alt="" width="50"></td>
                        </tr>
                    </tbody>';
                }
            }
            echo "</table>";
        } else {
            echo "No hay resultados";
        }
        //mysqli_close($link);
    }

    //public function altaarticulo($id,$titulo,$texto,$imagen){

    public function altaarticulo($titulo, $texto, $rutaimagen)
    {

        $datos = Cuenta::administrardatos();
        $id = $datos->id;
        $imagen = $rutaimagen;
        $link = SQL::conectarMySQL();

        $query = "INSERT INTO articulos(ID_CUENTA,TITULO,PRECIO,IMAGEN)
                  VALUES ('" . $id . "',
                          '" . $titulo . "',
                          '" . $texto . "',
                          '" . $imagen . "');";

        //echo $query;
        //die();

        $result = mysqli_query($link, $query);
        //mysqli_close($link);   

    }

    public function registroarticuloalta()
    {

        $datos = Cuenta::administrardatos();
        $id = $datos->id;
        $id_art = self::buscarmaxID();
        $accion = "alta";
        $horario = date('Y-m-d H:m:s');
        $link = SQL::conectarMySQL();

        $query = "INSERT INTO art_registro(ID_ARTICULO,ID_CUENTA,ACCION,HORARIO)
                  VALUES ('" . $id_art . "',
                          '" . $id . "',
                          '" . $accion . "',
                          '" . $horario . "');";

        $result = mysqli_query($link, $query);

        //echo $query;
        //die();

        //mysqli_close($link);   
    }

    public function verificaralta($titulo, $texto)
    {

        if (empty($titulo) || empty($texto)) :
            header("Location:index.php?secciones=altaarticulo&estado=vacio");
            die();
        endif;

        $info = [];
        $info["titulo"] = $titulo;
        $info["texto"] = $texto;

        return $info;
    }

    public function buscarmaxID()
    {

        $maximo = 0;

        $link = SQL::conectarMySQL();
        $query = 'SELECT ID_ARTICULO FROM articulos;';
        $result = mysqli_query($link, $query);
        $respuesta = array();

        if (0 < mysqli_num_rows($result)) {

            while ($row = $result->fetch_assoc()) {

                if ($row["ID_ARTICULO"] > $maximo) {

                    $maximo = $row["ID_ARTICULO"];
                }
            }
        } else {
            echo 'No hay resultados';
        }

        return $maximo + 1;

        mysqli_close($link);
    }

    public function tablamodifarticulos()
    {

        $link = SQL::conectarMySQL();
        $datos = Cuenta::administrardatos();
        $query = 'SELECT ID_ARTICULO,ID_CUENTA,TITULO,PRECIO,IMAGEN FROM articulos;';
        $result = mysqli_query($link, $query);

        if ($result->num_rows > 0) {

            echo '<table class="tabla">
                    <tr class="color1">
                        <th>ID ARTICULO</th>
                        <th>ID USUARIO</th>
                        <th>TITULO</th>
                        <th>PRECIO</th>
                        <th>IMAGEN</th>
                        <th>ACCION</th>
                    </tr>';

            while ($row = $result->fetch_assoc()) {

                if ($row["ID_CUENTA"] == $datos->id) {

                    echo '<tr class="color2">
                            <td>' . $row["ID_ARTICULO"] . '</td>
                            <td>' . $row["ID_CUENTA"] . '</td>
                            <td>' . $row["TITULO"] . '</td>
                            <td>' . $row["PRECIO"] . '</td>
                            <td><img src="../' . $row["IMAGEN"] . '" class="img-thumbnail" alt="" width="50"></td>
                            <td><a href="index.php?secciones=editar&id_art=' . $row["ID_ARTICULO"] . '" class="button">Editar</a></td>
                        </tr>';
                }
            }
            echo "</table>";
        } else {
            echo "No hay resultados";
        }

        //mysqli_close($link);
    }

    public function formmodifarticulos($id_art)
    {

        $link = SQL::conectarMySQL();
        $datos = Cuenta::administrardatos();
        $query = 'SELECT ID_ARTICULO,ID_CUENTA,TITULO,PRECIO,IMAGEN FROM articulos;';
        $result = mysqli_query($link, $query);

        if ($result->num_rows > 0) {

            while ($row = $result->fetch_assoc()) {

                if ($row["ID_CUENTA"] == $datos->id && $row["ID_ARTICULO"] == $id_art) {

                    echo ' 
                <div class="background">
                        <h2 class="text-left mb-5">Modificar Articulos</h2>
                            <form method="POST" action="secciones/procesarmodif.php" enctype="multipart/form-data">
                                <input type="hidden" 
                                    name="id" 
                                    value="' . $row["ID_ARTICULO"] . '">

                                    <div>
                                    <p class="text-left"> Ingrese nombre a modificar</p>
                                        <input type="text" 
                                            id="nombre" 
                                            name="titulo" 
                                            class="form-control mb-3"
                                            value="' . $row["TITULO"] . '">
                                    </div>

                                <div>
                                    <p class="text-left"> Ingrese imagen a modificar</p>
                                        <input 
                                            type="file" 
                                            accept="image/png, 
                                            image/jpeg, 
                                            image/gif" 
                                            name="imagen"
                                            class="form-control-file"
                                            /> 
                                </div>

                                <div>
                                    <img 
                                        src="../' . $row["IMAGEN"] . '" 
                                        class="img-thumbnail mb-3" 
                                        alt="" 
                                        width="50">
                                </div> 
                                <br />

                                <div>
                                <p class="text-left"> Ingrese precio a modificar</p>
                                    <input type="text" 
                                        id="texto" 
                                        name="texto" 
                                        class="form-control"
                                        value="' . $row["PRECIO"] . '">
                                </div>
                                <button type="submit" class="btn btn-danger mt-3">Modificar</button>
                            </form>
                        
                    </div>';
                }
            }
        } else {

            echo "No hay resultados";
        }

        //mysqli_close($link);
    }

    public function visualizararticulo($id_art){

        $link = SQL::conectarMySQL();
        $query = 'SELECT ID_ARTICULO,ID_CUENTA,TITULO,PRECIO,IMAGEN FROM articulos;';
        $result = mysqli_query($link, $query);

        if ($result->num_rows > 0) {

            while ($row = $result->fetch_assoc()) {

                if ($row["ID_ARTICULO"] == $id_art) {

                    echo '
                        <div class="card" style="width: 20rem;">
                            <img src="' . $row["IMAGEN"] . '" 
                                 class="img-thumbnail" 
                                 alt="" 
                                 width="360">

                                <div class="card-body">
                                    <h5 class="card-title">' . $row["TITULO"] . '</h5>
                                    <h3 class="card-title"> ' . $row["PRECIO"] . '</h3>
                                    <p class="card-text">Estas zapatillas de running son perfectas para que tu pequeño deportista corra y juegue en el colegio o el parque. La suela de caucho no deja marcas y proporciona un agarre excelente en gimnasios y canchas cubiertas. El exterior de cuero suave con las 3 Tiras selladas les confiere un look dinámico y deportivo.</p>
                                </div>
                        </div>
                    ';
                }
            }
        } else {
            echo "No hay resultados";
        }

        //mysqli_close($link);
    }

    public function modificararticulo($id_art, $titulo, $texto, $imagen)
    {

        $datos = Cuenta::administrardatos();
        $id = $datos->id;
        $link = SQL::conectarMySQL();
        $query = "UPDATE articulos
                  SET TITULO = '" . $titulo . "'
                     ,PRECIO = '" . $texto . "'
                     ,IMAGEN = '" . $imagen . "'
                  WHERE ID_ARTICULO = '" . $id_art . "' 
                  AND ID_CUENTA = '" . $id . "';";

        $result = mysqli_query($link, $query);

        mysqli_close($link);
    }

    public function registroarticulomodif($id_art)
    {

        $datos = Cuenta::administrardatos();
        $id = $datos->id;
        $accion = "modif";
        $horario = date('Y-m-d H:m:s');
        $link = SQL::conectarMySQL();
        $query = "INSERT INTO art_registro(ID_ARTICULO
                                          ,ID_CUENTA
                                          ,ACCION
                                          ,HORARIO)
                  VALUES ('" . $id_art . "',
                          '" . $id . "',
                          '" . $accion . "',
                          '" . $horario . "');";

        //echo $query;
        //die();

        $result = mysqli_query($link, $query);

        //mysqli_close($link);   

    }

    public function rankingAsc(){
        //ORDEN ASCENDENTE

        $maximo = 0;
        $link = SQL::conectarMySQL();
        $query = 'SELECT ID_CUENTA 
                  FROM ART_REGISTRO ORDERBY ID_CUENTA ASC;';
        $result = mysqli_query($link, $query);
        $respuesta = array();

        if (0 < mysqli_num_rows($result)) {

            while ($row = $result->fetch_assoc()) {

                if ($row["ID_CUENTA"] > $maximo) {

                    $maximo = $row["ID_CUENTA"];
                }
            }
        } else {
            echo 'No hay resultados';
        }

        return $maximo + 1;

        mysqli_close($link);
    }
    public function rankinDesc(){
        //ORDEN DESCENDENTE

        $minimo = 100000000000;
        $link = SQL::conectarMySQL();
        $query = 'SELECT ID_CUENTA FROM ART_REGISTRO ORDERBY ID_CUENTA DESC;';
        $result = mysqli_query($link, $query);
        $respuesta = array();

        if (0 < mysqli_num_rows($result)) {

            while ($row = $result->fetch_assoc()) {

                if ($row["ID_CUENTA"] > $minimo) {

                    $minimo = $row["ID_CUENTA"];
                }
            }
        } else {
            echo 'No hay resultados';
        }

        return $minimo + 1;

        mysqli_close($link);
    }

    public function eliminardirarticulo($dir)  {
        $result = false;
        if ($handle = opendir("$dir")) {
            $result = true;
            while ((($file = readdir($handle)) !== false) && ($result)) {
                if ($file != '.' && $file != '..') {
                    if (is_dir("$dir/$file")) {
                        $result = eliminar_directorio("$dir/$file");
                    } else {
                        $result = unlink("$dir/$file");
                    }
                }
            }
            closedir($handle);
            if ($result) {
                $result = rmdir($dir);
            }
        }
        return $result;
    }
}
