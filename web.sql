/*
 Navicat Premium Data Transfer

 Source Server         : conexion
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : web

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 07/08/2018 22:29:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for art_registro
-- ----------------------------
DROP TABLE IF EXISTS `art_registro`;
CREATE TABLE `art_registro`  (
  `ID_ARTICULO` int(255) NULL DEFAULT NULL,
  `ID_CUENTA` int(255) NULL DEFAULT NULL,
  `ACCION` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `HORARIO` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of art_registro
-- ----------------------------
INSERT INTO `art_registro` VALUES (1, 2, 'alta', '2018-08-08 02:08:23');
INSERT INTO `art_registro` VALUES (2, 4, 'alta', '2018-08-08 02:08:53');
INSERT INTO `art_registro` VALUES (3, 4, 'alta', '2018-08-08 02:08:14');
INSERT INTO `art_registro` VALUES (4, 3, 'alta', '2018-08-08 02:08:48');
INSERT INTO `art_registro` VALUES (5, 2, 'alta', '2018-08-08 02:08:17');
INSERT INTO `art_registro` VALUES (6, 2, 'alta', '2018-08-08 02:08:35');
INSERT INTO `art_registro` VALUES (7, 4, 'alta', '2018-08-08 02:08:23');
INSERT INTO `art_registro` VALUES (8, 3, 'alta', '2018-08-08 02:08:49');
INSERT INTO `art_registro` VALUES (9, 3, 'alta', '2018-08-08 02:08:06');
INSERT INTO `art_registro` VALUES (9, 3, 'modif', '2018-08-08 02:08:01');
INSERT INTO `art_registro` VALUES (10, 2, 'alta', '2018-08-08 02:08:05');
INSERT INTO `art_registro` VALUES (11, 2, 'alta', '2018-08-08 02:08:23');
INSERT INTO `art_registro` VALUES (12, 4, 'alta', '2018-08-08 03:08:14');

-- ----------------------------
-- Table structure for articulos
-- ----------------------------
DROP TABLE IF EXISTS `articulos`;
CREATE TABLE `articulos`  (
  `ID_ARTICULO` int(255) NOT NULL AUTO_INCREMENT,
  `ID_CUENTA` int(255) NULL DEFAULT NULL,
  `TITULO` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `PRECIO` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `IMAGEN` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_ARTICULO`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of articulos
-- ----------------------------
INSERT INTO `articulos` VALUES (1, 2, 'zapatos1', '$1.455', 'imagenes/zapato1.jpg');
INSERT INTO `articulos` VALUES (2, 4, 'zapatos2', '$1.785', 'imagenes/zapato2.jpg');
INSERT INTO `articulos` VALUES (3, 4, 'zapatos3', '$2.325', 'imagenes/zapato3.jpg');
INSERT INTO `articulos` VALUES (4, 3, 'zapatos4', '$1.155', 'imagenes/zapato4.jpg');
INSERT INTO `articulos` VALUES (5, 2, 'zapatos5', '$3.825', 'imagenes/zapato5.jpg');
INSERT INTO `articulos` VALUES (6, 2, 'zapatos6', '$2.800', 'imagenes/zapato6.jpg');
INSERT INTO `articulos` VALUES (7, 4, 'zapatos7', '$1.735', 'imagenes/zapato7.jpg');
INSERT INTO `articulos` VALUES (8, 3, 'zapatos8', '$1.005', 'imagenes/zapato8.jpg');
INSERT INTO `articulos` VALUES (9, 3, 'zapatos9', '$1.635', 'imagenes/zapato9.jpg');
INSERT INTO `articulos` VALUES (10, 2, 'zapatos10', '$2.325', 'imagenes/zapato10.jpg');

-- ----------------------------
-- Table structure for datos
-- ----------------------------
DROP TABLE IF EXISTS `datos`;
CREATE TABLE `datos`  (
  `ID_CUENTA` int(255) NULL DEFAULT NULL,
  `EMAIL_USUARIO` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `ULTIMA_CONEXION` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of datos
-- ----------------------------
INSERT INTO `datos` VALUES (2, 'rocio.alvarez1@davinci.edu.ar', '2018-11-08 02:08:36');
INSERT INTO `datos` VALUES (4, 'jonathan.gilabert@davinci.edu.ar', '2018-11-25 02:08:19');

-- ----------------------------
-- Table structure for registro
-- ----------------------------
DROP TABLE IF EXISTS `registro`;
CREATE TABLE `registro`  (
  `ID_USUARIO` int(255) NOT NULL AUTO_INCREMENT,
  `EMAIL_USUARIO` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `PASSWORD_USUARIO` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_USUARIO`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of registro
-- ----------------------------
INSERT INTO `registro` VALUES (1, 'admin@web.com', '$2y$10$qqWCAB1MTlAk0JWfAfDT6ObyWRskhCFuJutbRMfGFaKxl8H3eR3IC');
INSERT INTO `registro` VALUES (2, 'rocio.alvarez1@davinci.edu.ar', '$2y$10$N7IApDSlLOWmuT1p8xHRheogrdq1X87j9es6nQv.Fzu15CZFjKzke');
INSERT INTO `registro` VALUES (3, 'jonathan.gilabert@davinci.edu.ar', '$2y$10$1LHG5xRv.iX6EMIKncdCh.i87V4GChQQ2bG5R2K1V7i5VGs3hyxGy');


SET FOREIGN_KEY_CHECKS = 1;
